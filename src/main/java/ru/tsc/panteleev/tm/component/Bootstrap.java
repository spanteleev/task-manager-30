package ru.tsc.panteleev.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.panteleev.tm.api.repository.ICommandRepository;
import ru.tsc.panteleev.tm.api.repository.IProjectRepository;
import ru.tsc.panteleev.tm.api.repository.ITaskRepository;
import ru.tsc.panteleev.tm.api.repository.IUserRepository;
import ru.tsc.panteleev.tm.api.service.*;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.panteleev.tm.exception.system.CommandNotSupportedException;
import ru.tsc.panteleev.tm.model.User;
import ru.tsc.panteleev.tm.repository.CommandRepository;
import ru.tsc.panteleev.tm.repository.ProjectRepository;
import ru.tsc.panteleev.tm.repository.TaskRepository;
import ru.tsc.panteleev.tm.repository.UserRepository;
import ru.tsc.panteleev.tm.service.*;
import ru.tsc.panteleev.tm.util.DateUtil;
import ru.tsc.panteleev.tm.util.SystemUtil;
import ru.tsc.panteleev.tm.util.TerminalUtil;
import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.tsc.panteleev.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository,taskRepository,projectRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.tsc.panteleev.tm.command.AbstractCommand.class);
        classes.stream().filter(clazz -> !Modifier.isAbstract(clazz.getModifiers())
                                && AbstractCommand.class.isAssignableFrom(clazz))
                        .forEach(this::registry);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        @NotNull final AbstractCommand command = clazz.newInstance();
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename),pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void runStartupOperation() {
        initPID();
        backup.start();
        initDemoData();
        fileScanner.start();
        registryShutdownHookOperation();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
    }

    private void registryShutdownHookOperation() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
                backup.stop();
                fileScanner.stop();
            }
        });
    }
    
    private void initDemoData() {
        if (userService.getSize() > 0) return;
        userService.create("test", "test", "qwerty@qwerty.ru");
        userService.create("admin", "admin", Role.ADMIN);
        @Nullable final User admin = userService.findByLogin("admin");
        @Nullable final User test = userService.findByLogin("test");
        if (admin == null || test == null) return;
        @NotNull final String adminId = admin.getId();
        @NotNull final String testId = test.getId();
        projectService.create(adminId,"p3","333",
                DateUtil.toDate("29.08.2021"),DateUtil.toDate("29.08.2022"));
        projectService.create(adminId,"p1","111");
        projectService.create(testId,"p5","555",
                DateUtil.toDate("29.08.2021"),DateUtil.toDate("29.08.2022"));
        projectService.create(testId,"p4","444");
        projectService.create(testId,"p6","666");
        taskService.create(adminId,"t3", "333333",
                DateUtil.toDate("29.08.2021"),DateUtil.toDate("29.08.2022"));
        taskService.create(adminId,"t1", "111111");
        taskService.create(testId,"t5", "555555",
                DateUtil.toDate("29.08.2021"),DateUtil.toDate("29.08.2022"));
        taskService.create(testId,"t4", "444444");
        taskService.create(testId,"t6", "666666");
    }

    public void run(@Nullable String[] args) {
        if (runWithArgument(args))
            System.exit(0);
        runStartupOperation();
        while (true)
            runWithCommand();
    }

    public void runWithCommand() {
        try {
            System.out.println("ENTER COMMAND:");
            String command = TerminalUtil.nextLine();
            runWithCommand(command);
            loggerService.command(command);
        } catch (final Exception e) {
            loggerService.error(e);
        }
    }

    public void runWithCommand(@Nullable String command) {
        runWithCommand(command, true);
    }

    protected void runWithCommand(@Nullable String command, boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public boolean runWithArgument(@Nullable String[] args) {
        if (args == null || args.length == 0) return false;
        runWithArgument(args[0]);
        return true;
    }

    public void runWithArgument(@Nullable String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(argument);
        if (abstractCommand == null)
            throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

}
