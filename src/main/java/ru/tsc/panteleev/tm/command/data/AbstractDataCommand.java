package ru.tsc.panteleev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import ru.tsc.panteleev.tm.dto.Domain;
import ru.tsc.panteleev.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected static final String FILE_BINARY = "./data.bin";

    @NotNull
    protected static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_BACKUP = "./data.backup";

    @NotNull
    protected static final String FILE_JAXB_XML = "./data.jaxb.xml";

    @NotNull
    protected static final String FILE_JAXB_JSON = "./data.jaxb.json";

    @NotNull
    protected static final String FILE_FASTERXML_XML = "./data.fasterxml.xml";

    @NotNull
    protected static final String FILE_FASTERXML_JSON = "./data.fasterxml.json";

    @NotNull
    protected static final String FILE_FASTERXML_YAML = "./data.fasterxml.yaml";

    @NotNull
    protected static final String JAVAX_XML_BIND_CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    protected static final String ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    protected static final String APPLICATION_JSON = "application/json";

    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
        serviceLocator.getUserService().set(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[] {Role.ADMIN};
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
